﻿#include <iostream>
#include "sqrt.h"
using namespace std;


int main()
{
    setlocale(0, "");

    double a;
    double eps;
    cout << "Вычисление квадратного корня\n" << endl;
    cout << "Число:    "; cin >> a;
    cout << "Точность: "; cin >> eps;

    Sqrt sqrt;
    double result = sqrt.calculate(a, eps);

    cout << endl << "  sqrt(" << a << ") = " << result << endl << endl;

    system("pause");
    return 0;
}