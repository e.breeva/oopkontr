#pragma once
#include <iostream>
using namespace std;

class Sqrt {
private:
    double xn1 = 1;
    double xn;

public:
    double result;


    double calculate(double aParam, double epsParam) {
        xn1 = 1;
        do {
            xn = xn1;
            xn1 = (xn + aParam / xn) / 2;
        } while (fabs(xn1 - xn) >= epsParam);
        result = xn1;
        return result;
    }
};
