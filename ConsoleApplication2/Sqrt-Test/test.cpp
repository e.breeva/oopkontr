#include "pch.h"
#include "../ConsoleApplication2/sqrt.h"

TEST(Sqrt, calculate) {
	Sqrt sqrt;
	double result = sqrt.calculate(25, 1);
	EXPECT_NEAR(5.0, result, 0.02);
}