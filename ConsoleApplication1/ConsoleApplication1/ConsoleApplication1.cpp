﻿#include <iostream>
#include "poly.h"
using namespace std;


int main(int argc, char** argv)
{
    setlocale(0, "");

    Polynom poly;
    poly.addValues(argc, argv);
    poly.calculate();
    poly.print();
    poly.clear();

    system("pause");
    return 0;
}