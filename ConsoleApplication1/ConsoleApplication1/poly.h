#pragma once

#include <iostream>
#include <vector>
using namespace std;

class Polynom {
private:
    double sum = 0;
    double v = 3;
    vector<double> values;

public:    
    vector<double> getValues() {
        return values;
    }

    double getV() {
        return v;
    }

    void addValues(int valuesCount, char** initValues) {
        for (int i = 0; i < valuesCount; ++i) {
            cout << initValues[i] << endl; 
            if (strcmp(initValues[i], "-v") == 0) {
                i++;
                if (i >= valuesCount) break;
                double xV = strtod(initValues[i], NULL);
                if (errno == 0 && xV != 0.0) v = xV; 
            }
            else {
                double xV = strtod(initValues[i], NULL);
                if (errno == 0 && xV != 0) values.push_back(xV);
            }
        }
    }

    double calculate() {
        sum = 0;
        for (int i =0; i < values.size(); ++i) {
            sum += 1 / values[i] * v;
        }
        return sum;
    }

    void print() {
        cout << endl << sum << endl;
    }

    void clear() {
        sum = 0;
        values.clear();
    }
};