#include "pch.h"
#include "../ConsoleApplication1/poly.h"

TEST(Polynom, addValues) {
	Polynom poly;
	char** params = new char* [7];
	params[0] = "abc";
	params[1] = "1";
	params[2] = "3";
	params[3] = "0";
	params[4] = "5";
	params[5] = "-v";
	params[6] = "5";
	poly.addValues(7, params);
	vector<double> values = poly.getValues();
	EXPECT_EQ(3, values.size());
	EXPECT_EQ(5, poly.getV());
}
TEST(Polynom, calculate) {
	Polynom poly;
	char** params = new char* [7];
	params[0] = "abc";
	params[1] = "1";
	params[2] = "3";
	params[3] = "0";
	params[4] = "5";
	params[5] = "-v";
	params[6] = "3";
	poly.addValues(7, params);
	double result = poly.calculate();
	EXPECT_EQ(1.0 / 1.0 * 3.0 + 1.0 / 3.0 * 3.0 + 1.0 / 5.0 * 3.0, result);
}