import matplotlib.pyplot as plt
def graphic(nach,sma,med):
    grx=[]
    grx1=[]
    for i in range(len(nach)):
        grx.append(i)
    for i in range(len(med)):
        grx1.append(i)
    plt.xlabel('дни')
    plt.ylabel('цена')
    plt.grid()
    plt.plot(grx,nach,grx,sma,grx1,med)
    plt.legend(["до","после(sma)","после(med)"])
    plt.show()
