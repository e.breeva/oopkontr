﻿#include <iostream>

using namespace std;

class Sqrt {
private:
    double xn1 = 1;
    double xn = 0;

public:
    double a = 0;
    double eps = 0;
    double result = 0;

    void print() {
        cout << endl << "  sqrt(" << a << ") = " << result << endl << endl;
    }

    void calculate() {
        xn1 = 1;
        do {
            xn = xn1;
            xn1 = (xn + a / xn) / 2;
        } while (fabs(xn1 - xn) >= eps);
        result = xn1;
    }
};

int main()
{
    setlocale(0, "");

    Sqrt sqrt;

    cout << "Вычисление квадратного корня\n" << endl;
    cout << "Число:    "; cin >> sqrt.a;
    cout << "Точность: "; cin >> sqrt.eps;

    sqrt.calculate();
    sqrt.print();

    system("pause");
    return 0;
}