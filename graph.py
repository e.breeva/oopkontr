import matplotlib.pyplot as plt
def graphic(nach,posle,alg):
    grx=[]
    grx1=[]
    for i in range(len(nach)):
        grx.append(i)
    for i in range(len(posle)):
        grx1.append(i)
    plt.xlabel('дни')
    plt.ylabel('цена')
    plt.grid()
    plt.plot(grx,nach,grx1,posle)
    plt.legend(["до","после "+str(alg)])
    plt.show()
