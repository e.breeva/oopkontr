import requests
import collections
import matplotlib.pyplot as plt
import numpy as np 
req=requests.get('http://www.py4e.com/code3/mbox.txt').text

class pochta:
    def __init__(self, result,confidence,probability,auth):
        self.result=result
        self.probability= probability
        self.confidence=confidence
        self.auth=auth
        
    def show_all(self):
        print(self.auth)
        print("X-DSPAM-Probability: " + self.probability)
        print("X-DSPAM-Confidence: " + self.confidence)
        print("X-DSPAM-Result: " + self.result + "\n")
        
    def prob(self):
        return(float(self.probability))
    
    def authors(self):
        return(self.auth)
    
    def results(self):
        return(self.result)
    
def srznach(spisokvseh):
    sr=0
    for i in range(len(spisokvseh)-1):
       sr+= (spisokvseh[i].prob())
    return(sr/len(spisokvseh))

def spamers(spisokspamers):
    flag=0
    for i in range(len(spisokspamers)-1):
        if spisokspamers[i].results()!="Innocent":
            flag=1
            print("spamer")
            spisokspamers[i].show_all()
    if flag==0:
        print("no spamers")
        
def otpraviteli(spisokvseh):
    spisokotprav=[]
    for i in range(len(spisokvseh)-1):        
        spisokotprav.append(spisokvseh[i].authors())
    povtor=dict(collections.Counter(spisokotprav))
    return(povtor.keys(),povtor.values())
    
def graph(grx,gry):
    position = np.arange(len(grx))
    fig, ax = plt.subplots()
    ax.barh(position, gry)
    ax.set_yticks(position)
    labels = ax.set_yticklabels(grx,fontsize = 8)
    plt.grid()
    plt.ylabel('email')
    plt.xlabel('количество сообщений, шт')
    plt.show()
    
spisokvseh=[]
X_DSPAM_Result=""
X_DSPAM_Confidence=""
X_DSPAM_Probability=""
Author=""

for lines in req.splitlines():
    if lines.startswith("X-DSPAM-Result:"):
        X_DSPAM_Result=(lines.split("X-DSPAM-Result: ")[1])
    if lines.startswith("X-DSPAM-Confidence:"):
        X_DSPAM_Confidence=(lines.split("X-DSPAM-Confidence: ")[1])
    if lines.startswith("X-DSPAM-Probability:"):
        X_DSPAM_Probability=(lines.split("X-DSPAM-Probability: ")[1])
    if lines.startswith("Author:"):
        Author=(lines.split("Author: ")[1])
        polzov=pochta(X_DSPAM_Result,X_DSPAM_Confidence,X_DSPAM_Probability,Author)
        spisokvseh.append(polzov)
spamers(spisokvseh)
print("среднее X-DSPAM-Probability: "+str(srznach(spisokvseh)))
##spisok vseh otpravitelei, X_DSPAM_Confidence - stepen doveriya otpravitelu
##for i in range(len(spisokvseh)-1):
##    spisokvseh[i].show_all()
grx,gry=(otpraviteli(spisokvseh))
graph(grx,gry)

