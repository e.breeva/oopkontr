#include <iostream>

using namespace std;

class Polynom {
private:
    double sum = 0;
public:
    void add(double value) {
        if (value != 0) {
            sum += 1 / value * 3;
        }
    }

    void print() {
        cout << endl << sum << endl;
    }

    void clear() {
        sum = 0;
    }
};
int main(int argc, char** argv)
{
    Polynom poly;

    for (int i = 1; i < argc; ++i) {
        double param = strtod(argv[i], NULL);
        poly.add(param);
    }
    poly.print();
    poly.clear();

    system("pause");
    return 0;
}
